package ru.t1.rleonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.rleonov.tm.enumerated.UserSort;
import ru.t1.rleonov.tm.enumerated.Status;
import java.util.Collection;
import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    void clear(@Nullable String userId);

    void clear();

    void set(@NotNull Collection<M> models);

    @NotNull
    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    M removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    List<M> findAll(@Nullable String userId,
                    @Nullable UserSort userSort
    );

    @NotNull
    List<M> findAll();

}
