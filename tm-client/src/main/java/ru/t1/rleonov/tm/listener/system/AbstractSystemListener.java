package ru.t1.rleonov.tm.listener.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.listener.AbstractListener;
import ru.t1.rleonov.tm.enumerated.Role;
import java.util.List;

@Getter
@Setter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected List<AbstractListener> listeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
