package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataXmlJaxBRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadJaxBListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file using jaxb.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerLoadDataXmlJaxBRequest request = new ServerLoadDataXmlJaxBRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
