package ru.t1.rleonov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import java.util.List;

public interface IProjectRestEndpoint {

    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @PostMapping("/save")
    ProjectDTO save(@RequestBody ProjectDTO project);

    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void deleteById(@RequestBody ProjectDTO project);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<ProjectDTO> projects);

    @PostMapping("/clear")
    void clear();

}
