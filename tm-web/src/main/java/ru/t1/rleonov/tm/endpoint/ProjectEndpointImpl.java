package ru.t1.rleonov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @Override
    @PostMapping("/save")
    public ProjectDTO save(@RequestBody ProjectDTO project) {
        return projectService.save(project);
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") String id) {
        return projectService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") String id) {
        return projectService.findOneById(id) != null;
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        projectService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void deleteById(@RequestBody ProjectDTO project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@RequestBody List<ProjectDTO> projects) {
        projectService.clear(projects);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}
