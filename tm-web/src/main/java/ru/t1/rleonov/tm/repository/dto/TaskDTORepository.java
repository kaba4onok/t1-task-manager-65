package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.dto.TaskDTO;
import java.util.List;

@Repository
public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}
