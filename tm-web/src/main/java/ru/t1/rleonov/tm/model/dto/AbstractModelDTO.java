package ru.t1.rleonov.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.rleonov.tm.enumerated.Status;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractModelDTO {

    @Id
    @NotNull
    protected String id = UUID.randomUUID().toString();

    @Nullable
    @Column(nullable = true, name = "user_id")
    private String userId;

    @Column
    @NotNull
    protected String name = "";

    @Column
    @NotNull
    protected String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return name + " : " + status + " : " + description;
    }

}
