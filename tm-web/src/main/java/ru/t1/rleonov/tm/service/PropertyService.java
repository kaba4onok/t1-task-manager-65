package ru.t1.rleonov.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['database.second_lvl_cache']}")
    private String dbSecondLvlCash;

    @Value("#{environment['database.schema']}")
    private String dbSchema;

    @Value("#{environment['database.factory_class']}")
    private String dbFactoryClass;

    @Value("#{environment['database.use_query_cache']}")
    private String dbUseQueryCash;

    @Value("#{environment['database.use_min_puts']}")
    private String dbUseMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String dbRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    private String dbHazelConfig;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['database.dialect']}")
    private String dbDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dbDdlAuto;

    @Value("#{environment['database.show_sql']}")
    private String dbShowSql;

    @Value("#{environment['database.format_sql']}")
    private String dbFormatSql;

    @Value("#{environment['database.driver']}")
    private String dbDriver;

    @Value("#{environment['database.username']}")
    private String dbLogin;

    @Value("#{environment['database.password']}")
    private String dbPassword;

    @Value("#{environment['database.url']}")
    private String dbUrl;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @Value("#{environment['author.email']}")
    private String authorEmail;

    @Value("#{environment['author.name']}")
    private String authorName;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

}
