package ru.t1.rleonov.tm.service.web;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.web.ITaskService;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.NameEmptyException;
import ru.t1.rleonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.rleonov.tm.model.web.TaskWeb;
import ru.t1.rleonov.tm.repository.web.TaskWebRepository;

@Service
@NoArgsConstructor
public class TaskService extends AbstractService<TaskWeb>
        implements ITaskService {

    @NotNull
    @Autowired
    public TaskWebRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskWeb save(@NotNull final TaskWeb task) {
        if (task.getId().isEmpty()) throw new IdEmptyException();
        if (task.getName() == null || task.getName().isEmpty()) throw new NameEmptyException();
        return repository.save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public void deleteByProjectId(@NotNull final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        repository.deleteByProjectId(projectId);
    }

}
